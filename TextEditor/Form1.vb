﻿Imports System.Drawing.Printing
Imports System.IO
Public Class Form1
    Private Sub OpenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenToolStripMenuItem.Click
        If OpenFileDialog.ShowDialog() = DialogResult.OK Then
            Using StreamReader As New StreamReader(OpenFileDialog.FileName)
                Try
                    RichTextBox.Rtf = StreamReader.ReadToEnd()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Info:", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Try
            End Using
        End If
    End Sub

    Private Sub SaveAsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaveAsToolStripMenuItem.Click
        If SaveFileDialog.ShowDialog() = DialogResult.OK Then
            Try
                RichTextBox.SaveFile(SaveFileDialog.FileName)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Info:", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End If
    End Sub

    Private Sub StyleAndSizeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles StyleAndSizeToolStripMenuItem.Click
        FontDialog.ShowDialog()
        RichTextBox.Font = FontDialog.Font
    End Sub

    Private Sub ColorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ColorToolStripMenuItem.Click
        ColorDialog.ShowDialog()
        RichTextBox.ForeColor = ColorDialog.Color
    End Sub

    Private Sub PrintToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintToolStripMenuItem.Click
        If PrintDialog.ShowDialog() = DialogResult.OK Then
            PrintDocument.Print()
        End If
    End Sub

    Private Sub PrintDocument_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintDocument.PrintPage
        Using b As New SolidBrush(ColorDialog.Color), f = FontDialog.Font, g = e.Graphics
            g.DrawString(RichTextBox.Text, f, b, 20, 20)
        End Using
    End Sub

    Private Sub PrintPreviewToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintPreviewToolStripMenuItem.Click
        PrintPreviewDialog.Document = PrintDocument
        PrintPreviewDialog.ShowDialog()
    End Sub
End Class
